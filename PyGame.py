#!/usr/bin/python3
import pygame

pygame.init()

display_width = 800
display_height = 600

gameDisplay = pygame.display.set_mode( (800, 600) )

pygame.display.set_caption('Ship')

clock = pygame.time.Clock()

crashed = False

image = pygame.image.load('rocket.png').convert_alpha()
image = pygame.transform.scale(image,(100,100))

bulletImage = pygame.image.load('bullet.png').convert_alpha()

class MovingImage:
	img = None
	x = 0
	y = 0
	vx = 0
	vy = 0
	rotation = 0
	visible = True

	def __init__(self, image):
		self.img = image

	def draw(self):
		if self.visible:
			gameDisplay.blit(self.img, (self.x, self.y))

	def move(self):
		self.x += self.vx;
		self.y += self.vy;

	def setVX(self,nvx):
		self.vx = nvx

	def setVY(self,nvy):
		self.vy = nvy

bullet = MovingImage(bulletImage)
bullet.visible = False

player = MovingImage(image)

while not crashed:
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			crashed = True

		if event.type == pygame.KEYDOWN:
			if event.key == 273: #UP
				player.setVY(-2)

			if event.key == 274: #DOWN
				player.setVY(2)

			if event.key == 275: #RIGHT
				player.setVX(2)

			if event.key == 276: #LEFT
				player.setVX(-2)

			if event.key == 32: #Space/shoot
				bullet.visible = True
				bullet.x = player.x + 50
				bullet.y = player.y
				bullet.setVY(-20)
				bullet.setVX(0)

		if event.type == pygame.KEYUP:
			if event.key == 273 or event.key == 274: #UP or DOWN
				player.setVY(0)

			if event.key == 275 or event.key == 276: #RIGHT or LEFT
				player.setVX(0)

	player.move()
	bullet.move()

	gameDisplay.fill((0,0,0))
	player.draw()
	bullet.draw()
	print(event)

	pygame.display.update()
	clock.tick(60)

pygame.quit()
quit()